#include "MeshReconstructionWindow.h"
#include "GraspPlanning/Visualization/CoinVisualization/CoinConvexHullVisualization.h"
#include "GraspPlanning/ContactConeGenerator.h"
#include "GraspPlanning/MeshConverter.h"
#include "VirtualRobot/EndEffector/EndEffector.h"
#include "VirtualRobot/Workspace/Reachability.h"
#include "VirtualRobot/ManipulationObject.h"
#include "VirtualRobot/Grasping/Grasp.h"
#include "VirtualRobot/IK/GenericIKSolver.h"
#include "VirtualRobot/Grasping/GraspSet.h"
#include "VirtualRobot/CollisionDetection/CDManager.h"
#include "VirtualRobot/XML/ObjectIO.h"
#include "VirtualRobot/XML/RobotIO.h"
#include "VirtualRobot/Visualization/CoinVisualization/CoinVisualizationFactory.h"
#include "VirtualRobot/Visualization/TriMeshModel.h"
#include "VirtualRobot/Visualization/VisualizationFactory.h"
#include <VirtualRobot/Import/RobotImporterFactory.h>
#include <VirtualRobot/RuntimeEnvironment.h>
#include <VirtualRobot/Visualization/VisualizationNode.h>
#include <GraspPlanning/ConvexHullGenerator.h>
#include <QFileDialog>
#include <QObject>
#include <Eigen/Geometry>

#include <time.h>
#include <vector>
#include <iostream>
#include <cmath>

#include <Inventor/actions/SoLineHighlightRenderAction.h>
#include <Inventor/nodes/SoShapeHints.h>
#include <Inventor/nodes/SoLightModel.h>
#include <Inventor/sensors/SoTimerSensor.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/nodes/SoMatrixTransform.h>
#include <Inventor/nodes/SoScale.h>
#include <Inventor/nodes/SoMaterial.h>

#include "Visualization/CoinVisualization/CGALCoinVisualization.h"
#include "CGALMeshConverter.h"
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Scale_space_surface_reconstruction_3.h>
#include <CGAL/Point_set_3.h>
#include <CGAL/Point_set_3/IO.h>
#include <CGAL/Polygon_mesh_processing/corefinement.h>
#include <CGAL/Polygon_mesh_processing/orientation.h>
#include <sstream>


using namespace std;
using namespace VirtualRobot;
using namespace GraspStudio;
using namespace SimoxCGAL;

float TIMER_MS = 30.0f;

MeshReconstructionWindow::MeshReconstructionWindow(std::string& objectFile)
    : QMainWindow(NULL)
{
    VR_INFO << " start " << std::endl;

    // init the random number generator
    srand(time(NULL));

    this->objectFile = objectFile;


    sceneSep = new SoSeparator;
    sceneSep->ref();
    objectSep = new SoSeparator;
    sceneSep->addChild(objectSep);
    pointsSep = new SoSeparator;
    sceneSep->addChild(pointsSep);
    reconstructionSep = new SoSeparator;
    sceneSep->addChild(reconstructionSep);
    setupUI();

    loadObject(objectFile);

    buildVisu();
    viewer->viewAll();
}


MeshReconstructionWindow::~MeshReconstructionWindow()
{
    sceneSep->unref();
}



void MeshReconstructionWindow::setupUI()
{
    UI.setupUi(this);
    viewer = new SoQtExaminerViewer(UI.frameViewer, "", TRUE, SoQtExaminerViewer::BUILD_POPUP);

    // setup
    viewer->setBackgroundColor(SbColor(1.0f, 1.0f, 1.0f));


    viewer->setGLRenderAction(new SoLineHighlightRenderAction);
    viewer->setTransparencyType(SoGLRenderAction::SORTED_OBJECT_BLEND);
    viewer->setFeedbackVisibility(true);
    viewer->setSceneGraph(sceneSep);
    viewer->viewAll();
    viewer->setAccumulationBuffer(true);
    viewer->setAntialiasing(true, 4);

    connect(UI.pushButtonLoadPoints, SIGNAL(clicked()), this, SLOT(loadPoints()));
    connect(UI.pushButtonReconstruct, SIGNAL(clicked()), this, SLOT(reconstruct()));
    connect(UI.pushButtonLoadObject, SIGNAL(clicked()), this, SLOT(loadObject()));
    connect(UI.pushButtonSave, SIGNAL(clicked()), this, SLOT(save()));
    connect(UI.pushButtonRegularize, SIGNAL(clicked()), this, SLOT(regularize()));
    connect(UI.pushButtonNormals, SIGNAL(clicked()), this, SLOT(computeNormals()));
    connect(UI.checkBoxColModel, SIGNAL(clicked()), this, SLOT(colModel()));
    connect(UI.checkBoxObject, SIGNAL(clicked()), this, SLOT(buildVisu()));
    connect(UI.checkBoxPoints, SIGNAL(clicked()), this, SLOT(buildVisu()));
    connect(UI.checkBoxReconstruction, SIGNAL(clicked()), this, SLOT(buildVisu()));
    connect(UI.checkBoxNormalsReconstr, SIGNAL(clicked()), this, SLOT(buildVisu()));
    connect(UI.checkBoxNormalsObj, SIGNAL(clicked()), this, SLOT(buildVisu()));
    connect(UI.pushButtonConfineToChull, SIGNAL(clicked()), this, SLOT(confineToChull()));
    connect(UI.pushButtonReconstructAndConfine, SIGNAL(clicked()), this, SLOT(reconstructAndConfine()));

}

void MeshReconstructionWindow::updateInfo()
{
    VR_INFO << "Updating info\n";
    std::stringstream ss;
    ss << std::setprecision(3);
    int nrTriangles = 0;
    if (object && object->getVisualization() && object->getVisualization()->getTriMeshModel())
    {
        nrTriangles = object->getVisualization()->getTriMeshModel()->faces.size();
    }
    int recTri = 0;
    if (trimesh)
    {
        recTri = trimesh->faces.size();
    }
    ss << "Nr Points: " << points.size() << "\nObject\n  Triangles: " << nrTriangles << "\nReconstruction\n Triangles:" << recTri;

    UI.labelInfo->setText(QString(ss.str().c_str()));
}


void MeshReconstructionWindow::resetSceneryAll()
{
    updateInfo();
}


void MeshReconstructionWindow::closeEvent(QCloseEvent* event)
{
    quit();
    QMainWindow::closeEvent(event);
}


SoSeparator* MeshReconstructionWindow::drawNormals(TriMeshModelPtr t)
{
    SoSeparator* res = new SoSeparator;
    SoUnits* u = new SoUnits();
    u->units = SoUnits::MILLIMETERS;
    res->addChild(u);
    Eigen::Vector3f z(0, 0, 1.0f);
    SoSeparator* arrow = CoinVisualizationFactory::CreateArrow(z, 10.0f, 0.8f);
    arrow->ref();

    if (t->normals.size() > 0)
    {
        for (size_t i = 0; i < t->faces.size(); i++)
        {
            unsigned int id1 = t->faces[i].id1;
            unsigned int id2 = t->faces[i].id2;
            unsigned int id3 = t->faces[i].id3;
            Eigen::Vector3f& v1 = t->vertices[id1];
            Eigen::Vector3f& v2 = t->vertices[id2];
            Eigen::Vector3f& v3 = t->vertices[id3];

            unsigned int normalIndx1 = t->faces[i].idNormal1;
            unsigned int normalIndx2 = t->faces[i].idNormal2;
            unsigned int normalIndx3 = t->faces[i].idNormal3;
            Eigen::Vector3f& normal1 = t->normals[normalIndx1];
            Eigen::Vector3f& normal2 = t->normals[normalIndx2];
            Eigen::Vector3f& normal3 = t->normals[normalIndx3];

            if (fabs(normal1.norm() - 1.0f) > 1.1)
            {
                VR_ERROR << "Wrong normal, norm:" << normal1.norm() << std::endl;
            }

            if (fabs(normal2.norm() - 1.0f) > 1.1)
            {
                VR_ERROR << "Wrong normal, norm:" << normal2.norm() << std::endl;
            }

            if (fabs(normal3.norm() - 1.0f) > 1.1)
            {
                VR_ERROR << "Wrong normal, norm:" << normal3.norm() << std::endl;
            }

            SoMatrixTransform* mt1 = new SoMatrixTransform;
            SoMatrixTransform* mt2 = new SoMatrixTransform;
            SoMatrixTransform* mt3 = new SoMatrixTransform;

            MathTools::Quaternion q1 = MathTools::getRotation(z, normal1);
            MathTools::Quaternion q2 = MathTools::getRotation(z, normal2);
            MathTools::Quaternion q3 = MathTools::getRotation(z, normal3);
            Eigen::Matrix4f mat1 = MathTools::quat2eigen4f(q1);
            Eigen::Matrix4f mat2 = MathTools::quat2eigen4f(q2);
            Eigen::Matrix4f mat3 = MathTools::quat2eigen4f(q3);
            mat1.block(0, 3, 3, 1) = v1;
            mat2.block(0, 3, 3, 1) = v2;
            mat3.block(0, 3, 3, 1) = v3;
            SbMatrix m1(reinterpret_cast<SbMat*>(mat1.data()));
            SbMatrix m2(reinterpret_cast<SbMat*>(mat2.data()));
            SbMatrix m3(reinterpret_cast<SbMat*>(mat3.data()));
            mt1->matrix.setValue(m1);
            mt2->matrix.setValue(m2);
            mt3->matrix.setValue(m3);
            SoSeparator* sn1 = new SoSeparator();
            sn1->addChild(mt1);
            sn1->addChild(arrow);
            res->addChild(sn1);
            SoSeparator* sn2 = new SoSeparator();
            sn2->addChild(mt2);
            sn2->addChild(arrow);
            res->addChild(sn2);
            SoSeparator* sn3 = new SoSeparator();
            sn3->addChild(mt3);
            sn3->addChild(arrow);
            res->addChild(sn3);
        }
    }
    else
    {
        for (size_t i = 0; i < t->faces.size(); i++)
        {
            unsigned int id1 = t->faces[i].id1;
            unsigned int id2 = t->faces[i].id2;
            unsigned int id3 = t->faces[i].id3;
            Eigen::Vector3f& v1 = t->vertices[id1];
            Eigen::Vector3f& v2 = t->vertices[id2];
            Eigen::Vector3f& v3 = t->vertices[id3];
            Eigen::Vector3f v = (v1 + v2 + v3) / 3.0f;

            Eigen::Vector3f& normal1 = t->faces[i].normal;

            if (fabs(normal1.norm() - 1.0f) > 1.1)
            {
                VR_ERROR << "Wrong normal, norm:" << normal1.norm() << std::endl;
                continue;
            }


            SoMatrixTransform* mt1 = new SoMatrixTransform;
            MathTools::Quaternion q1 = MathTools::getRotation(z, normal1);
            Eigen::Matrix4f mat1 = MathTools::quat2eigen4f(q1);
            mat1.block(0, 3, 3, 1) = v;
            SbMatrix m1(reinterpret_cast<SbMat*>(mat1.data()));
            mt1->matrix.setValue(m1);
            SoSeparator* sn1 = new SoSeparator();
            sn1->addChild(mt1);
            sn1->addChild(arrow);
            res->addChild(sn1);
        }
    }

    arrow->unref();
    reconstructionSep->addChild(res);
    return res;
}

void MeshReconstructionWindow::buildVisu()
{
    VR_INFO << "building visu\n";
    objectSep->removeAllChildren();
    if (object && UI.checkBoxObject->isChecked())
    {
        SceneObject::VisualizationType colModel2 = (UI.checkBoxColModel->isChecked()) ? SceneObject::Collision : SceneObject::Full;
        SoNode* n = CoinVisualizationFactory::getCoinVisualization(object, colModel2);
        if (n)
        {
            SoSeparator* n2 = new SoSeparator;
            //SoMaterial* color = new SoMaterial();
            //color->transparency = 0.7f;
            //color->diffuseColor.setIgnored(TRUE);
            // color->setOverride(TRUE);
            // n2->addChild(color);
            n2->addChild(n);
            objectSep->addChild(n2);
        }
        else
        {
            VR_INFO << "failed to create a visualization for object\n";
        }
    }
    if (object && object->getVisualization() && object->getVisualization()->getTriMeshModel() && UI.checkBoxNormalsObj->isChecked())
    {
        objectSep->addChild(drawNormals(object->getVisualization()->getTriMeshModel()));
    }

    reconstructionSep->removeAllChildren();
    if (reconstructedObject && UI.checkBoxReconstruction->isChecked())
    {
        SceneObject::VisualizationType colModel2 = (UI.checkBoxColModel->isChecked()) ? SceneObject::Collision : SceneObject::Full;
        SoNode* n = CoinVisualizationFactory::getCoinVisualization(reconstructedObject, colModel2);
        if (n)
        {
            SoSeparator* n2 = new SoSeparator;
            /*SoMaterial* color = new SoMaterial();
             color->transparency = 0.7f;
             color->diffuseColor.setIgnored(TRUE);
             color->setOverride(TRUE);
             n2->addChild(color);*/
            n2->addChild(n);
            reconstructionSep->addChild(n2);
        }
        else
        {
            VR_INFO << "failed to create a visualization for reconstructedObject\n";
        }
    }
    if (trimesh && UI.checkBoxNormalsReconstr->isChecked())
    {
        reconstructionSep->addChild(drawNormals(trimesh));
    }



    pointsSep->removeAllChildren();
    if (points.size() > 0 && UI.checkBoxPoints->isChecked())
    {
        SoNode* n = CoinVisualizationFactory::CreateVerticesVisualization(points, 4.0f);
        if (n)
        {
            /*SoMaterial* color = new SoMaterial();
            color->transparency = 0.7f;
            color->diffuseColor.setIgnored(TRUE);
            color->setOverride(TRUE);
            pointsSep->addChild(color);*/
            pointsSep->addChild(n);
        }
        else
        {
            VR_INFO << "failed to create a visualization for points\n";
        }
    }

    viewer->scheduleRedraw();
    viewer->viewAll();
}

int MeshReconstructionWindow::main()
{
    SoQt::show(this);
    SoQt::mainLoop();
    return 0;
}


void MeshReconstructionWindow::quit()
{
    std::cout << "GraspPlannerWindow: Closing" << std::endl;
    this->close();
    SoQt::exitMainLoop();
}

void MeshReconstructionWindow::loadPoints()
{
    resetSceneryAll();
    //QString fi = QFileDialog::getOpenFileName(this, tr("Open Object"), QString(), tr("XML Files (*.xml)"));
    QString fi;
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::ExistingFile);
    dialog.setAcceptMode(QFileDialog::AcceptOpen);
    QStringList nameFilters;
    nameFilters << "xyz Files (*.xyz)";
    dialog.setNameFilters(nameFilters);

    if (dialog.exec())
    {
        if (dialog.selectedFiles().size() == 0)
        {
            return;
        }

        fi = dialog.selectedFiles()[0];
    }
    else
    {
        VR_INFO << "load dialog canceled" << std::endl;
        return;
    }

    string filename = std::string(fi.toLatin1());
    if (filename.empty())
    {
        return;
    }

    //load points
    {
        objectFile = filename + ".xml";
        object = nullptr;

        points.clear();
        normals.clear();

        std::ifstream in{filename};
        CGAL::Point_set_3<CGAL::Epick::Point_3> pointset;

        if (!CGAL::read_xyz_point_set(in, pointset))
        {
            VR_INFO << "Failed to read: " << filename << '\n';
            return ;
        }
        points.clear();
        for (const auto& p : pointset.points())
        {
            points.emplace_back(p.x(), p.y(), p.z());
        }

        VR_INFO << "Computing normals for " << points.size() << " points\n";
        computeNormals();

        if (points.size() != normals.size())
        {
            throw std::logic_error{"compute normals generates not one normal per point"};
        }

        {
            VR_INFO << "Using convex hull as object model\n";
            const auto ch = GraspStudio::ConvexHullGenerator::CreateConvexHull(points);

            TriMeshModelPtr t{new TriMeshModel{*ch}};
            object = ManipulationObject::createFromMesh(t);
            object->setFilename(objectFile);
            object->setName("object");
        }

        trimesh.reset();
        reconstructedObject.reset();

        buildVisu();
        updateInfo();
    }
}

void MeshReconstructionWindow::confineToChull()
{
    //from https://doc.cgal.org/latest/Polygon_mesh_processing/Polygon_mesh_processing_2corefinement_mesh_union_and_intersection_8cpp-example.html
    if (!trimesh)
    {
        return;
    }
    const auto ch = GraspStudio::ConvexHullGenerator::CreateConvexHull(points);
    auto chmesh = std::make_shared<TriMeshModel>(*ch);

    const auto sm_trimesh = CGALMeshConverter::ConvertToSurfaceMesh(trimesh);
    const auto sm_chmesh = CGALMeshConverter::ConvertToSurfaceMesh(chmesh);

    //calc intersect
    {
        namespace PMP = CGAL::Polygon_mesh_processing;
        namespace params = CGAL::Polygon_mesh_processing::parameters;

        SimoxCGAL::SurfaceMesh out_union, out_intersection;

        std::array<boost::optional<SimoxCGAL::SurfaceMesh*>, 4> output;
        output[PMP::Corefinement::UNION] = &out_union;
        output[PMP::Corefinement::INTERSECTION] = &out_intersection;

        // for the example, we explicit the named parameters, this is identical to
        // PMP::corefine_and_compute_boolean_operations(mesh1, mesh2, output)
        std::array<bool, 4> res =
            PMP::corefine_and_compute_boolean_operations(
                *(sm_trimesh->getMesh()), *(sm_chmesh->getMesh()),
                output,
                params::all_default(), // mesh1 named parameters
                params::all_default(), // mesh2 named parameters
                std::make_tuple(
                    params::vertex_point_map(get(boost::vertex_point, out_union)), // named parameters for out_union
                    params::vertex_point_map(get(boost::vertex_point, out_intersection)), // named parameters for out_intersection
                    params::all_default(), // named parameters for mesh1-mesh2 not used
                    params::all_default()) // named parameters for mesh2-mesh1 not used)
            );
        if (!res[PMP::Corefinement::INTERSECTION])
        {
            VR_INFO << "Intersection could not be computed\n";
            return;
        }

        VR_INFO << "Intersection was successfully computed\n";
        if(CGAL::is_closed(out_intersection))
        {
            CGAL::Polygon_mesh_processing::orient(out_intersection);
        }
        else
        {
            VR_INFO << "can't orient the mesh since it is not clodes\n";
        }
        trimesh =  CGALMeshConverter::ConvertCGALMesh(out_intersection);
    }

    reconstructedObject = VirtualRobot::ManipulationObject::createFromMesh(trimesh);

    buildVisu();
}

void MeshReconstructionWindow::reconstructAndConfine()
{
    doReconstruction(true);
}

void MeshReconstructionWindow::reconstruct()
{
    doReconstruction(false);
}

void MeshReconstructionWindow::loadObject()
{
    resetSceneryAll();
    //QString fi = QFileDialog::getOpenFileName(this, tr("Open Object"), QString(), tr("XML Files (*.xml)"));
    QString fi;
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::ExistingFile);
    dialog.setAcceptMode(QFileDialog::AcceptOpen);
    QStringList nameFilters;
    nameFilters << "XML Files (*.xml)"
                << "Manipulation Files (*.moxml)"
                << "All Files (*.*)";
    dialog.setNameFilters(nameFilters);

    if (dialog.exec())
    {
        if (dialog.selectedFiles().size() == 0)
        {
            return;
        }

        fi = dialog.selectedFiles()[0];
    }
    else
    {
        VR_INFO << "load dialog canceled" << std::endl;
        return;
    }

    string file = std::string(fi.toLatin1());
    if (file.empty())
    {
        return;
    }

    loadObject(file);
}

bool MeshReconstructionWindow::updateNormals(TriMeshModelPtr t)
{
    if (!t)
    {
        return false;
    }
    int size = t->vertices.size();
    int faceCount = t->faces.size();
    std::vector<std::set<MathTools::TriangleFace*>> vertex2FaceMap(size);
    t->normals.resize(size);
    for (int j = 0; j < faceCount; ++j)
    {
        MathTools::TriangleFace& face = t->faces.at(j);
        vertex2FaceMap[face.id1].insert(&t->faces.at(j));
        vertex2FaceMap[face.id2].insert(&t->faces.at(j));
        vertex2FaceMap[face.id3].insert(&t->faces.at(j));
    }
    int noNormals = 0;
    for (size_t i = 0; i < vertex2FaceMap.size(); i++)
    {
        std::set<MathTools::TriangleFace*>& fs = vertex2FaceMap.at(i);
        Eigen::Vector3f n;
        n.setZero();
        if (fs.size() == 0)
        {
            //VR_WARNING << "No normal?!" << std::endl;
            noNormals++;
            n << 1.0f, 0, 0;
            t->normals[i] = n;
            continue;
        }

        for (MathTools::TriangleFace* tf : fs)
        {
            n += tf->normal;
        }
        n /= fs.size();
        t->normals[i] = n;
    }

    // update normalID
    for (int j = 0; j < faceCount; ++j)
    {
        MathTools::TriangleFace& face = t->faces.at(j);
        face.idNormal1 = face.id1;
        face.idNormal2 = face.id2;
        face.idNormal3 = face.id3;
    }
    VR_INFO << "Created " << size - noNormals << " normals. Skipped " << noNormals << " unconnected vertices." << std::endl;

    return true;
}

void MeshReconstructionWindow::loadObject(const std::string& filename)
{
    objectFile = filename;

    try
    {
        object = ObjectIO::loadManipulationObject(objectFile);
    }
    catch (...)
    {
        VR_ERROR << "could not load file " << objectFile << std::endl;
        return;
    }
    if (!object)
    {
        return;
    }

    trimesh.reset();
    points.clear();
    normals.clear();
    reconstructedObject.reset();

    // extract points
    if (object && object->getVisualization() && object->getVisualization()->getTriMeshModel())
    {
        TriMeshModelPtr t = object->getVisualization()->getTriMeshModel();
        t->mergeVertices();
        if (t->vertices.size() != t->normals.size())
        {
            VR_INFO << "Updating normals, points.size=" << t->vertices.size() << " != normals.size=" << t->normals.size() << std::endl;
            if (!updateNormals(t))
            {
                object.reset();
                buildVisu();
                return;
            }
        }
        points = t->vertices;
        normals = t->normals;
    }

    buildVisu();

    updateInfo();
}

void MeshReconstructionWindow::colModel()
{
    buildVisu();
}

void MeshReconstructionWindow::regularize()
{
    if (!object || points.size() == 0)
    {
        return;
    }

    reconstruction.reset(new SimoxCGAL::MeshReconstruction());
    reconstruction->setVerbose(true);

    reconstruction->regularizePoints(points, normals, 1.0f);

    buildVisu();
    updateInfo();
}


void MeshReconstructionWindow::computeNormals()
{
    if (points.size() == 0)
    {
        return;
    }

    reconstruction.reset(new SimoxCGAL::MeshReconstruction());
    reconstruction->setVerbose(true);

    reconstruction->computeNormals(points, normals, true);

    if (object)
    {
        buildVisu();
        updateInfo();
    }
}


void MeshReconstructionWindow::doReconstruction(bool doConfine)
{
    if (!object || points.size() == 0)
    {
        return;
    }
    reconstruction.reset(new SimoxCGAL::MeshReconstruction());
    reconstruction->setVerbose(true);

    const double sm_angle = UI.doubleSpinBox_sm_angle->value();
    const double sm_radius = UI.doubleSpinBox_sm_radius->value();
    const double sm_distance = UI.doubleSpinBox_sm_distance->value();

    VR_INFO << "reconstructing " << points.size() << " / " << normals.size()
            << " points / normals using sm_angle = " << sm_angle
            << ", sm_radius = " << sm_radius
            << ", sm_distance = " << sm_distance
            << std::endl;

    reconstruction->possionOptions.sm_angle    = sm_angle;
    reconstruction->possionOptions.sm_radius   = sm_radius;
    reconstruction->possionOptions.sm_distance = sm_distance;
    PolyhedronMeshPtr m = doConfine ?
                          reconstruction->reconstructMeshPoissonAndConfineToConvexHull(points, normals) :
                          reconstruction->reconstructMeshPoisson(points, normals);
    CGALPolyhedronMeshPtr mesh(new CGALPolyhedronMesh(m));
    trimesh = CGALMeshConverter::ConvertCGALMesh(mesh);

    if (trimesh)
    {
        reconstructedObject = VirtualRobot::ManipulationObject::createFromMesh(trimesh);
        if (object)
        {
            reconstructedObject->setName(object->getName());
        }
    }
    buildVisu();
    updateInfo();
}

void MeshReconstructionWindow::save()
{
    if (!reconstructedObject)
    {
        return;
    }

    //QString fi = QFileDialog::getSaveFileName(this, tr("Save ManipulationObject"), QString(), tr("XML Files (*.xml)"));
    QString fi;
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::AnyFile);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    QStringList nameFilters;
    nameFilters << "Manipulation Files (*.moxml)"
                << "All Files (*.*)";
    dialog.setNameFilters(nameFilters);

    if (dialog.exec())
    {
        if (dialog.selectedFiles().size() == 0)
        {
            return;
        }

        fi = dialog.selectedFiles()[0];
    }
    else
    {
        VR_INFO << "save dialog canceled" << std::endl;
        return;
    }

    std::string objectFile = std::string(fi.toLatin1());
    bool ok = false;

    try
    {
        std::filesystem::path filenameBaseComplete(objectFile);
        std::filesystem::path filenameBasePath = filenameBaseComplete.parent_path();
        std::string basePath = filenameBasePath.string();
        std::filesystem::path filenameBase = filenameBaseComplete.filename();

        std::string fnSTL = filenameBase.stem().string() + ".stl";
        std::string fn = basePath + "/" + fnSTL;

        ObjectIO::writeSTL(trimesh, fn, reconstructedObject->getName());

        if (reconstructedObject->getVisualization())
        {
            reconstructedObject->getVisualization()->setFilename(fnSTL, false);
        }
        if (reconstructedObject->getCollisionModel() && reconstructedObject->getCollisionModel()->getVisualization())
        {
            reconstructedObject->getCollisionModel()->getVisualization()->setFilename(fnSTL, false);
        }
        ok = ObjectIO::saveManipulationObject(reconstructedObject, objectFile);
    }
    catch (VirtualRobotException& e)
    {
        std::cout << " ERROR while saving object" << std::endl;
        std::cout << e.what();
        return;
    }

    if (!ok)
    {
        std::cout << " ERROR while saving object" << std::endl;
        return;
    }
}
