#include "MeshReconstruction.h"

#include <VirtualRobot/Visualization/TriMeshModel.h>
#include <GraspPlanning/ConvexHullGenerator.h>

#include <CGAL/Surface_mesh_default_triangulation_3.h>
#include <CGAL/Implicit_surface_3.h>
#include <CGAL/property_map.h>
#include <CGAL/Poisson_reconstruction_function.h>
#include <CGAL/compute_average_spacing.h>
#include <CGAL/IO/output_surface_facets_to_polyhedron.h>
#include <CGAL/Scale_space_surface_reconstruction_3.h>
#include <CGAL/grid_simplify_point_set.h>
#include <CGAL/pca_estimate_normals.h>
#include <CGAL/mst_orient_normals.h>
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Polygon_mesh_processing/corefinement.h>
#include <CGAL/Polygon_mesh_processing/orientation.h>

#include <CGALMeshConverter.h>

#include <vector>
#include <fstream>
#include <utility>
#include <functional>

using namespace VirtualRobot;
using namespace std;

namespace SimoxCGAL::detail
{
    void confine(const std::vector<Eigen::Vector3f>& points, SimoxCGAL::PolyhedronMesh& mesh)
    {
        SimoxCGAL::SurfaceMesh sm_mesh;

        CGAL::copy_face_graph(mesh, sm_mesh);

        namespace PMP = CGAL::Polygon_mesh_processing;
        namespace params = CGAL::Polygon_mesh_processing::parameters;

        const auto ch = GraspStudio::ConvexHullGenerator::CreateConvexHull(points);
        auto chmesh = std::make_shared<TriMeshModel>(*ch);
        const auto sm_chmesh = SimoxCGAL::CGALMeshConverter::ConvertToSurfaceMesh(chmesh);

        SimoxCGAL::SurfaceMesh out_union, out_intersection;
        std::array<boost::optional<SimoxCGAL::SurfaceMesh*>, 4> output;
        output[PMP::Corefinement::UNION] = &out_union;
        output[PMP::Corefinement::INTERSECTION] = &out_intersection;

        // for the example, we explicit the named parameters, this is identical to
        // PMP::corefine_and_compute_boolean_operations(mesh1, mesh2, output)
        std::array<bool, 4> res =
            PMP::corefine_and_compute_boolean_operations(
                sm_mesh, *(sm_chmesh->getMesh()),
                output,
                params::all_default(), // mesh1 named parameters
                params::all_default(), // mesh2 named parameters
                std::make_tuple(
                    params::vertex_point_map(get(boost::vertex_point, out_union)), // named parameters for out_union
                    params::vertex_point_map(get(boost::vertex_point, out_intersection)), // named parameters for out_intersection
                    params::all_default(), // named parameters for mesh1-mesh2 not used
                    params::all_default()) // named parameters for mesh2-mesh1 not used)
            );
        if (!res[PMP::Corefinement::INTERSECTION])
        {
            VR_INFO << "Intersection could not be computed\n";
            throw std::invalid_argument{"Intersection could not be computed"};
        }
        VR_INFO << "Intersection was successfully computed\n";
        if(CGAL::is_closed(out_intersection))
        {
            CGAL::Polygon_mesh_processing::orient(out_intersection);
            VR_INFO << "Intersection was successfully oriented\n";
        }
        else
        {
            VR_INFO << "can't orient the mesh since it is not clodes\n";
        }
        SimoxCGAL::PolyhedronMesh p2;
        CGAL::copy_face_graph(out_intersection, p2);
        mesh = std::move(p2);
    }
}

namespace SimoxCGAL
{

    MeshReconstruction::MeshReconstruction(): verbose(false)
    {
    }


    MeshReconstruction::~MeshReconstruction()
    {

    }

    bool MeshReconstruction::regularizePoints(std::vector<Eigen::Vector3f>& points, std::vector<Eigen::Vector3f>& normals, float cellSize)
    {
        typedef KernelPolyhedron::Point_3 Point;
        typedef KernelPolyhedron::Vector_3 Vector;

        if (points.size() == 0)
        {
            return false;
        }

        if (points.size() != normals.size() && normals.size() > 0)
        {
            return false;
        }

        std::vector<Point> pointsC;
        std::vector<Vector> normalsC;
        if (verbose)
        {
            VR_INFO << points.size() << " input points" << std::endl;
        }

        std::vector<std::size_t> indices(points.size());
        for (std::size_t i = 0; i < points.size(); ++i)
        {
            pointsC.push_back(Point(points.at(i)[0], points.at(i)[1], points.at(i)[2]));
            if (normals.size() > 0)
            {
                normalsC.push_back(Vector(normals.at(i)[0], normals.at(i)[1], normals.at(i)[2]));
            }
            indices[i] = i;
        }
        // simplification by clustering using erase-remove idiom
        std::vector<std::size_t>::iterator end;
        {
            CGAL::Iterator_range range {indices.begin(), indices.end()};
            end = CGAL::grid_simplify_point_set(
                      range,
                      cellSize,
                      CGAL::parameters::point_map(CGAL::make_property_map(pointsC))
                  );
        }
        std::size_t k = end - indices.begin();
        if (verbose)
        {
            VR_INFO << "Keep " << k << " of " << indices.size() <<  " indices" << std::endl;
        }


        std::vector<Eigen::Vector3f> tmp_points(k);
        std::vector<Eigen::Vector3f> tmp_normals(k);
        for (std::size_t i = 0; i < k; ++i)
        {
            tmp_points[i] = points[indices[i]];
            if (normals.size() > 0)
            {
                tmp_normals[i] = normals[indices[i]];
            }
        }
        points.swap(tmp_points);
        normals.swap(tmp_normals);
        return true;
    }

    bool MeshReconstruction::computeNormals(std::vector<Eigen::Vector3f>& points, std::vector<Eigen::Vector3f>& storeNormals, bool erasePointsWithWrongNormal)
    {

        // Point with normal vector stored in a std::pair.
        typedef std::pair<Point, Vector> PointVectorPair;

        // Concurrency
#ifdef CGAL_LINKED_WITH_TBB
        typedef CGAL::Parallel_tag Concurrency_tag;
#else
        typedef CGAL::Sequential_tag Concurrency_tag;
#endif
        std::list<PointVectorPair> cgPoints;
        for (size_t i = 0; i < points.size(); i++)
        {
            const Eigen::Vector3f& p = points.at(i);
            cgPoints.push_back(std::make_pair(Point(p[0], p[1], p[2]), Vector(0, 0, 0)));
        }

        // Estimates normals direction.
        // Note: pca_estimate_normals() requires an iterator over points
        // as well as property maps to access each point's position and normal.
        const int nb_neighbors = 18; // K-nearest neighbors = 3 rings
        {
            CGAL::Iterator_range range{cgPoints.begin(), cgPoints.end()};
            CGAL::pca_estimate_normals<Concurrency_tag>(
                range,
                nb_neighbors,
                CGAL::parameters::point_map(CGAL::First_of_pair_property_map<PointVectorPair>())
                .normal_map(CGAL::Second_of_pair_property_map<PointVectorPair>())
            );
        }

        // Orients normals.
        // Note: mst_orient_normals() requires an iterator over points
        // as well as property maps to access each point's position and normal.
        std::list<PointVectorPair>::iterator unoriented_points_begin;
        {
            CGAL::Iterator_range range{cgPoints.begin(), cgPoints.end()};
            unoriented_points_begin = CGAL::mst_orient_normals(
                                          range,
                                          nb_neighbors,
                                          CGAL::parameters::point_map(CGAL::First_of_pair_property_map<PointVectorPair>())
                                          .normal_map(CGAL::Second_of_pair_property_map<PointVectorPair>())
                                      );
        }
        // Optional: delete points with an unoriented normal
        // if you plan to call a reconstruction algorithm that expects oriented normals.
        if (erasePointsWithWrongNormal)
        {
            cgPoints.erase(unoriented_points_begin, cgPoints.end());
        }


        size_t origPointSize = points.size();

        storeNormals.clear();
        if (erasePointsWithWrongNormal)
        {
            points.clear();
        }
        for (PointVectorPair& p : cgPoints)
        {
            if (erasePointsWithWrongNormal)
            {
                points.push_back(Eigen::Vector3f(p.first[0], p.first[1], p.first[2]));
            }
            storeNormals.push_back(Eigen::Vector3f(p.second[0], p.second[1], p.second[2]));
        }

        VR_INFO << "Oriented " << storeNormals.size() << " normals. Could not coherently orient " << origPointSize - storeNormals.size() << " normals." << std::endl;

        VR_ASSERT(points.size() == storeNormals.size());
        return true;
    }

    void MeshReconstruction::setVerbose(bool v)
    {
        verbose = v;
    }

    PolyhedronMeshPtr MeshReconstruction::reconstructMeshPoisson(
        const std::vector<Eigen::Vector3f>& points,
        const std::vector<Eigen::Vector3f>& normals,
        bool parameterFillHoles)
    {
        if (points.size() != normals.size())
        {
            VR_ERROR << "Size of points != size of normals" << std::endl;
            return PolyhedronMeshPtr();
        }
        if (points.size() == 0)
        {
            return PolyhedronMeshPtr();
        }
        if (verbose)
        {
            VR_INFO << "Converting " << points.size() << " points with normals to cgal data structure" << std::endl;
        }

        std::vector<PointNormalPoly> pointsNormals;
        pointsNormals.reserve(points.size());
        for (size_t i = 0; i < points.size(); i++)
        {
            const Eigen::Vector3f& p = points.at(i);
            const Eigen::Vector3f& n = normals.at(i);
            PointNormalPoly::Vector v(n[0], n[1], n[2]);
            PointNormalPoly pn(p[0], p[1], p[2], v);
            pointsNormals.push_back(pn);
        }

        return reconstructMeshPoisson(pointsNormals, parameterFillHoles);
    }


    PolyhedronMeshPtr MeshReconstruction::reconstructMeshPoisson(std::vector<PointNormalPoly>& points, bool parameterFillHoles)
    {
        if (points.size() == 0)
        {
            VR_ERROR << "no points to reconstruct..." << std::endl;
            return PolyhedronMeshPtr();
        }

        if (verbose)
        {
            VR_INFO << "Starting to reconstruct mesh" << std::endl;
        }


        if (verbose)
        {
            VR_INFO << "Creating implicit function.." << std::endl;
        }

        // Creates implicit function from the read points using the default solver.
        // Note: this method requires an iterator over points
        // + property maps to access each point's position and normal.
        // The position property map can be omitted here as we use iterators over Point_3 elements.
        CGAL::Poisson_reconstruction_function<KernelPolyhedron> function(points.begin(), points.end(),
                CGAL::make_normal_of_point_with_normal_map(std::vector<PointNormalPoly>::value_type()));
        // Computes the Poisson indicator function f()
        // at each vertex of the triangulation.
        if (! function.compute_implicit_function(parameterFillHoles))
        {
            VR_ERROR << "Could not compute implicit function..." << std::endl;
            return PolyhedronMeshPtr();
        }
        if (verbose)
        {
            VR_INFO << "Compute average spacing.." << std::endl;
        }
        // Computes average spacing
        KernelPolyhedron::FT average_spacing = CGAL::compute_average_spacing<CGAL::Sequential_tag>(
                CGAL::Iterator_range{points.begin(), points.end()}, 6 /* knn = 1 ring */);
        // Gets one point inside the implicit surface
        // and computes implicit function bounding sphere radius.
        PointNormalPoly inner_point = function.get_inner_point();
        KernelPolyhedron::Sphere_3 bsphere = function.bounding_sphere();
        KernelPolyhedron::FT radius = std::sqrt(bsphere.squared_radius());

        if (verbose)
        {
            VR_INFO << "Computing surface.." << std::endl;
        }

        // Defines the implicit surface: requires defining a
        // conservative bounding sphere centered at inner point.
        KernelPolyhedron::FT sm_sphere_radius = 5.0 * radius;
        KernelPolyhedron::FT sm_dichotomy_error = possionOptions.sm_distance * average_spacing / 1000.0; // Dichotomy error must be << sm_distance
        CGAL::Implicit_surface_3< KernelPolyhedron, CGAL::Poisson_reconstruction_function<KernelPolyhedron> > surface(function,
                KernelPolyhedron::Sphere_3(inner_point, sm_sphere_radius * sm_sphere_radius),
                sm_dichotomy_error / sm_sphere_radius);
        // Defines surface mesh generation criteria
        CGAL::Surface_mesh_default_criteria_3<CGAL::Surface_mesh_default_triangulation_3> criteria(
            possionOptions.sm_angle,  // Min triangle angle (degrees)
            possionOptions.sm_radius * average_spacing, // Max triangle size
            possionOptions.sm_distance * average_spacing); // Approximation error
        if (verbose)
        {
            VR_INFO << "Generating mesh.." << std::endl;
        }
        // Generates surface mesh with manifold option
        CGAL::Surface_mesh_default_triangulation_3 tr; // 3D Delaunay triangulation for surface mesh generation
        CGAL::Surface_mesh_complex_2_in_triangulation_3<CGAL::Surface_mesh_default_triangulation_3> c2t3(tr); // 2D complex in 3D Delaunay triangulation
        CGAL::make_surface_mesh(c2t3,                                 // reconstructed mesh
                                surface,                              // implicit surface
                                criteria,                             // meshing criteria
                                CGAL::Manifold_with_boundary_tag());  // require manifold mesh
        if (tr.number_of_vertices() == 0)
        {
            VR_ERROR << "Could not compute surface..." << std::endl;
            return PolyhedronMeshPtr();
        }

        if (verbose)
        {
            VR_INFO << "Converting mesh to polyhedron..." << std::endl;
        }
        // saves reconstructed surface mesh
        //std::ofstream out("kitten_poisson-20-30-0.375.off");
        PolyhedronMeshPtr result(new PolyhedronMesh());
        CGAL::output_surface_facets_to_polyhedron(c2t3, *result);
        //out << output_mesh;
        return result;
    }


    PolyhedronMeshPtr MeshReconstruction::reconstructMeshPoissonAndConfineToConvexHull(std::vector<PointNormalPoly>& points, bool parameterFillHoles)
    {
        PolyhedronMeshPtr rec = reconstructMeshPoisson(points, parameterFillHoles);
        std::vector<Eigen::Vector3f> epoints;
        epoints.reserve(points.size());
        for (const auto& pwn : points)
        {
            const auto& p = pwn.position();
            epoints.emplace_back(p.x(), p.y(), p.z());
        }
        detail::confine(epoints, *rec);
        return rec;
    }
    PolyhedronMeshPtr MeshReconstruction::reconstructMeshPoissonAndConfineToConvexHull(const std::vector<Eigen::Vector3f>& points, const std::vector<Eigen::Vector3f>& normals, bool parameterFillHoles)
    {
        PolyhedronMeshPtr rec = reconstructMeshPoisson(points, normals, parameterFillHoles);
        detail::confine(points, *rec);
        return rec;
    }

}
