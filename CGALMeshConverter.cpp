#include "CGALMeshConverter.h"

#include <Eigen/Geometry>  // For cross()

#include <VirtualRobot/Visualization/TriMeshModel.h>
#include <CGAL/Polygon_mesh_processing/orient_polygon_soup.h>
#include <CGAL/Polygon_mesh_processing/polygon_soup_to_polygon_mesh.h>
#include <CGAL/Polygon_mesh_processing/orientation.h>

#include <boost/smart_ptr/make_shared.hpp>

#ifndef Q_MOC_RUN
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/properties.hpp>
#include <boost/iterator/transform_iterator.hpp>

#include <CGAL/boost/graph/helpers.h>
#endif

using namespace VirtualRobot;

namespace SimoxCGAL
{


    CGALPolyhedronMeshBuilder::CGALPolyhedronMeshBuilder(VirtualRobot::TriMeshModelPtr& tm)
        : tm(tm)
    {
    }

    void CGALPolyhedronMeshBuilder::operator()(PolyhedronMesh::HalfedgeDS& hds)
    {
        if (!tm)
        {
            return;
        }
        CGAL::Polyhedron_incremental_builder_3<PolyhedronMesh::HalfedgeDS> B(hds, true);
        //B.ABSOLUTE_INDEXING; macht keinen Unterschied!?! nicht wichtig?
        std::size_t numberVertices = tm->vertices.size();
        std::size_t numberFaces = tm->faces.size();

        typedef typename PolyhedronMesh::HalfedgeDS::Vertex Vertex;
        typedef typename Vertex::Point Point;

        B.begin_surface(numberVertices, numberFaces);
        //add all vertex to cgal
        for (std::vector<Eigen::Vector3f>::iterator itVertices = tm->vertices.begin(); itVertices != tm->vertices.end(); itVertices++)
        {
            Eigen::Vector3f tmp = *itVertices;
            Point p1 = Point(tmp(0), tmp(1), tmp(2));
            B.add_vertex(p1);
        }

        int nrWrongFacet = 0;

        //add face structure
        for (std::vector<VirtualRobot::MathTools::TriangleFace>::iterator itFace = tm->faces.begin(); itFace != tm->faces.end(); itFace++)
        {
            // write new InputIterator to test if facet is valid
            std::vector<std::size_t> test_facet;
            test_facet.push_back(itFace->id1);
            test_facet.push_back(itFace->id2);
            test_facet.push_back(itFace->id3);
            if (B.test_facet(test_facet.begin(), test_facet.end()))
            {
                B.begin_facet();
                B.add_vertex_to_facet(itFace->id1);
                B.add_vertex_to_facet(itFace->id2);
                B.add_vertex_to_facet(itFace->id3);
                B.end_facet();
            }
            else
            {
                nrWrongFacet++;
            }
        }
        B.end_surface();
        //std::cout << "Nummer der nicht hinzugefuegten Facet ist: " << nrWrongFacet << std::endl;
    }

    SimoxCGAL::CGALPolyhedronMeshPtr SimoxCGAL::CGALMeshConverter::ConvertToPolyhedronMesh(
        VirtualRobot::TriMeshModelPtr tm,
        bool trimeshAlreadyCGALCompatible,
        bool verbose)
    {
        VirtualRobot::TriMeshModelPtr tm2 = tm;
        if (!trimeshAlreadyCGALCompatible)
        {
            if (verbose)
            {
                VR_INFO << "Converting tm to compatible structure" << std::endl;
            }
            tm2 = SimoxCGAL::CGALMeshConverter::ConvertTrimeshCGALCompatible(tm, verbose);
            if (verbose)
            {
                VR_INFO << "Converting tm to compatible structure...done" << std::endl;
            }
        }

        if (verbose)
        {
            VR_INFO << "Converting tm to cgal polyhedron mesh structure" << std::endl;
        }


        CGALPolyhedronMeshPtr res(new CGALPolyhedronMesh(PolyhedronMeshPtr(new PolyhedronMesh())));
        CGALPolyhedronMeshBuilder b(tm2);
        PolyhedronMeshPtr mesh = res->getMesh();
        mesh->delegate(b);

        return res;
    }

    template<class P>
    CGAL::Surface_mesh<P>
    ConvertToSurfaceMesh_CGAL(const VirtualRobot::TriMeshModel& tm, bool trimeshAlreadyCGALCompatible = false, bool verbose = true)
    {
        const auto conv = [verbose](const auto & mesh)
        {
            if (verbose)
            {
                VR_INFO << "Converting tm to cgal surface mesh structure" << std::endl;
            }

            using sm_t = CGAL::Surface_mesh<P>;
            sm_t sm;

            std::vector<typename sm_t::vertex_index> vidx;
            vidx.reserve(mesh.vertices.size());
            for (const auto& v : mesh.vertices)
            {
                vidx.emplace_back(sm.add_vertex({v(0), v(1), v(2)}));
            }
            for (const auto& f : mesh.faces)
            {
                sm.add_face(vidx.at(f.id1), vidx.at(f.id2), vidx.at(f.id3));
            }
            if (verbose)
            {
                VR_INFO << "Converting tm to cgal data structure...done" << std::endl;
            }
            return sm;
        };
        if (trimeshAlreadyCGALCompatible)
        {
            return conv(tm);
        }

        if (verbose)
        {
            VR_INFO << "Converting tm to compatible structure" << std::endl;
        }
        auto tm2 = SimoxCGAL::CGALMeshConverter::ConvertTrimeshCGALCompatible(tm, verbose);
        if (verbose)
        {
            VR_INFO << "Converting tm to compatible structure...done" << std::endl;
        }
        return conv(tm2);
    }

    CGAL::Surface_mesh<CGAL::Point_3<CGAL::Epick>>
            CGALMeshConverter::ConvertToSurfaceMesh_Epick(VirtualRobot::TriMeshModelPtr tm, bool trimeshAlreadyCGALCompatible, bool verbose)
    {
        return ConvertToSurfaceMesh_CGAL<CGAL::Point_3<CGAL::Epick>>(*tm, trimeshAlreadyCGALCompatible, verbose);
    }
    CGAL::Surface_mesh<CGAL::Point_3<CGAL::Epick>>
            CGALMeshConverter::ConvertToSurfaceMesh_Epick(const VirtualRobot::TriMeshModel& tm, bool trimeshAlreadyCGALCompatible, bool verbose)
    {
        return ConvertToSurfaceMesh_CGAL<CGAL::Point_3<CGAL::Epick>>(tm, trimeshAlreadyCGALCompatible, verbose);
    }
    CGAL::Surface_mesh<CGAL::Point_3<CGAL::Simple_cartesian<double>>>
    CGALMeshConverter::ConvertToSurfaceMesh_Cartesian(VirtualRobot::TriMeshModelPtr tm, bool trimeshAlreadyCGALCompatible, bool verbose)
    {
        return ConvertToSurfaceMesh_CGAL<CGAL::Point_3<CGAL::Simple_cartesian<double>>>(*tm, trimeshAlreadyCGALCompatible, verbose);
    }
    CGAL::Surface_mesh<CGAL::Point_3<CGAL::Simple_cartesian<double>>>
    CGALMeshConverter::ConvertToSurfaceMesh_Cartesian(const VirtualRobot::TriMeshModel& tm, bool trimeshAlreadyCGALCompatible, bool verbose)
    {
        return ConvertToSurfaceMesh_CGAL<CGAL::Point_3<CGAL::Simple_cartesian<double>>>(tm, trimeshAlreadyCGALCompatible, verbose);
    }

    SimoxCGAL::CGALSurfaceMeshPtr SimoxCGAL::CGALMeshConverter::ConvertToSurfaceMesh(
        VirtualRobot::TriMeshModelPtr tm,
        bool trimeshAlreadyCGALCompatible,
        bool verbose)
    {
        return boost::make_shared<CGALSurfaceMesh>(
                   boost::make_shared<SurfaceMesh>(
                       ConvertToSurfaceMesh_Cartesian(tm, trimeshAlreadyCGALCompatible, verbose)
                   ));
    }

    TriMeshModelPtr CGALMeshConverter::ConvertCGALMesh(CGALSurfaceMeshPtr m)
    {
        VR_ASSERT(m);
        SurfaceMeshPtr mesh = m->getMesh();
        VR_ASSERT(mesh);
        return ConvertCGALMesh(*mesh);
    }

    template<class PT>
    TriMeshModelPtr conv_sm(const CGAL::Surface_mesh<PT>& mesh)
    {
        using mesh_t = CGAL::Surface_mesh<PT>;
        using point_t = PT;
        TriMeshModelPtr res(new TriMeshModel());
        std::map<size_t, int> idMap;


        for (auto index : mesh.vertices())
        {
            const point_t& a = mesh.point(index);
            const size_t indxCGAL = static_cast<size_t>(index);

            idMap[indxCGAL] = res->addVertex(
                                  static_cast<float>(a[0]),
                                  static_cast<float>(a[1]),
                                  static_cast<float>(a[2]));
        }
        typename mesh_t::Vertex_around_face_iterator fb, fe;
        typename mesh_t::Halfedge_index hi;

        {
            std::vector<int> polIndex;
            for (auto index : mesh.faces())
            {
                polIndex.clear();
                {
                    hi = mesh.halfedge(index);
                    fb = mesh.vertices_around_face(hi).begin();
                    fe = mesh.vertices_around_face(hi).end();
                    for (; fb != fe; ++fb)
                    {
                        polIndex.push_back(static_cast<size_t>(*fb));
                    }

                }
                if (polIndex.size() != 3)
                {
                    VR_ERROR << "polygon with # edges!=3 is not supported: edge count " << polIndex.size() << std::endl;
                    continue;
                }

                MathTools::TriangleFace f;
                f.id1 = polIndex.at(0);
                f.id2 = polIndex.at(1);
                f.id3 = polIndex.at(2);

                res->addFace(f);
            }
        }
        return res;
    }
    TriMeshModelPtr CGALMeshConverter::ConvertCGALMesh(const SurfaceMesh& mesh)
    {
        return conv_sm(mesh);
    }
    TriMeshModelPtr CGALMeshConverter::ConvertCGALMesh(const CGAL::Surface_mesh<CGAL::Point_3<CGAL::Epick>>& m)
    {
        return conv_sm(m);
    }

    TriMeshModelPtr CGALMeshConverter::ConvertCGALMesh(CGALPolyhedronMeshPtr m)
    {
        VR_ASSERT(m);
        VR_ASSERT(m->getMesh());

        TriMeshModelPtr res(new TriMeshModel);
        std::map<PolyVertexConstHandle, unsigned int> pointMap;

        for (PolyhedronMesh::Facet_const_iterator facet_it = m->getMesh()->facets_begin(); facet_it != m->getMesh()->facets_end(); ++facet_it)
        {

            //access vertices
            std::vector<unsigned int> vertices;

            PolyhedronMesh::Halfedge_around_facet_const_circulator half_it = facet_it->facet_begin();

            do
            {

                if (pointMap.find(half_it->vertex()) != pointMap.end())
                {
                    vertices.push_back(pointMap.at(half_it->vertex()));
                }
                else
                {
                    PointPoly tmp_in_point = half_it->vertex()->point();
                    Eigen::Vector3f tmp_out_point(tmp_in_point.x(), tmp_in_point.y(), tmp_in_point.z());
                    unsigned int vid = res->addVertex(tmp_out_point);
                    vertices.push_back(vid);
                    pointMap[half_it->vertex()] = vid;
                }
                //PointPoly tmp_in_point = half_it->vertex()->point();
                //Eigen::Vector3f tmp_out_point(tmp_in_point.x(), tmp_in_point.y(), tmp_in_point.z());
                //vertices.push_back(tmp_out_point);
            }
            while (++half_it != facet_it->facet_begin());
            if (vertices.size() > 2)
            {
                res->addFace(vertices.at(0), vertices.at(1), vertices.at(2));
                MathTools::TriangleFace& f = res->faces.at(res->faces.size() - 1);
                f.normal = TriMeshModel::CreateNormal(res->vertices.at(vertices.at(0)), res->vertices.at(vertices.at(1)), res->vertices.at(vertices.at(2)));
                //res->addTriangleWithFace(vertices.at(0), vertices.at(1), vertices.at(2));
                //TODO write to a right structure with neighboorhood relationships
            }
        }
        return res;
    }

    VirtualRobot::TriMeshModelPtr SimoxCGAL::CGALMeshConverter::ConvertTrimeshCGALCompatible(
        VirtualRobot::TriMeshModelPtr tm, bool verbose)
    {
        if (!tm)
        {
            throw std::invalid_argument("SimoxCGAL::CGALMeshConverter::ConvertTrimeshCGALCompatible: tm was null");
        }
        return std::make_shared<VirtualRobot::TriMeshModel>(CGALMeshConverter::ConvertTrimeshCGALCompatible(*tm, verbose));
    }

    VirtualRobot::TriMeshModel SimoxCGAL::CGALMeshConverter::ConvertTrimeshCGALCompatible(
        const VirtualRobot::TriMeshModel& tm, bool verbose)
    {
        TriMeshModel result;
        //copy data
        result.normals   = tm.normals;
        result.materials = tm.materials;
        result.colors    = tm.colors;

        for (const auto& faceOld : tm.faces)
        {
            bool found1 = false;
            bool found2 = false;
            bool found3 = false;
            int newPos1 = -1;
            int newPos2 = -1;
            int newPos3 = -1;
            auto itVerticesOld = tm.vertices.begin();
            Eigen::Vector3f candidateVertex1 = *(itVerticesOld + faceOld.id1);
            Eigen::Vector3f candidateVertex2 = *(itVerticesOld + faceOld.id2);
            Eigen::Vector3f candidateVertex3 = *(itVerticesOld + faceOld.id3);
            for (std::vector<Eigen::Vector3f>::iterator itVerticesNew = result.vertices.begin(); itVerticesNew != result.vertices.end(); itVerticesNew++)
            {
                if ((*itVerticesNew - candidateVertex1).norm() < 0.00001)
                {
                    found1 = true;
                    newPos1 = std::distance(result.vertices.begin(), itVerticesNew);
                }
                if ((*itVerticesNew - candidateVertex2).norm() < 0.000001)
                {
                    found2 = true;
                    newPos2 = std::distance(result.vertices.begin(), itVerticesNew);
                }
                if ((*itVerticesNew - candidateVertex3).norm() < 0.000001)
                {
                    found3 = true;
                    newPos3 = std::distance(result.vertices.begin(), itVerticesNew);
                }
            }
            //Eventuelle füge die alten vertices zu den neuen hinzu und specheiere Position
            if (!found1)
            {
                result.addVertex(candidateVertex1);
                newPos1 = result.vertices.size() - 1;
            }
            if (!found2)
            {
                result.addVertex(candidateVertex2);
                newPos2 = result.vertices.size() - 1;
            }
            if (!found3)
            {
                result.addVertex(candidateVertex3);
                newPos3 = result.vertices.size() - 1;
            }
            //create new Face
            MathTools::TriangleFace temp;
            if ((newPos1 >= -1) && (newPos2 >= -1) && (newPos3 >= -1))
            {
                std::vector<Eigen::Vector3f>::iterator itVerticesNew = result.vertices.begin();
                //test for clockwise or counterclockwise
                Eigen::Vector3f p1p2 = *(itVerticesNew + newPos2) - *(itVerticesNew + newPos1);
                Eigen::Vector3f p3p1 = *(itVerticesNew + newPos1) - *(itVerticesNew + newPos3);
                Eigen::Vector3f cross = p1p2.cross(p3p1);
                float l = cross.norm();
                //Punkte müssen so hinzugefügt werden, das die Halfedges gegen den Uhrzeigersinn angeordnet sind, wenn das polyhedron von aussen gesehen wird
                if (l != 0)
                {
                    cross /= l;
                    Eigen::Vector3f diff = faceOld.normal - cross;
                    if (diff.norm() <= 0.1)
                    {
                        temp.set(newPos1, newPos3, newPos2);
                    }
                    else
                    {
                        temp.set(newPos1, newPos2, newPos3);
                    }

                }
                else
                {
                    if (verbose)
                    {
                        VR_INFO << "Error cross product is equal to zero" << std::endl;
                    }
                    temp.set(newPos1, newPos3, newPos2);
                }

            }
            else
            {
                VR_ERROR << "Error in write to CgalStructure, one newPos is smaller than zero " << std::endl;
            }
            temp.setColor(faceOld.idColor1, faceOld.idColor2, faceOld.idColor3);
            temp.setNormal(faceOld.idNormal1, faceOld.idNormal2, faceOld.idNormal3);
            temp.normal = faceOld.normal;
            temp.setMaterial(faceOld.idMaterial);
            result.addFace(temp);
        }

        return result;
    }

    CGALPolyhedronMeshPtr CGALMeshConverter::convertSurface2PolyhedronMesh(CGALSurfaceMeshPtr s)
    {
        VR_ASSERT(s);

        PolyhedronMeshPtr p(new PolyhedronMesh());
        CGAL::copy_face_graph(*(s->getMesh()), *p);

        CGALPolyhedronMeshPtr res(new CGALPolyhedronMesh(p));
        return res;
    }
    /*
    CGALPolyhedronMeshPtr CGALMeshConverter::PolygonSoupToPolyhedronMesh(TriMeshModelPtr tm)
    {
        std::vector<KernelPolyhedron::Point_3> points;
        std::vector< std::vector<std::size_t> > polygons;

        for (size_t i=0;i<tm->vertices.size();i++)
        {
            Eigen::Vector3f &r = tm->vertices.at(i);
            KernelPolyhedron::Point_3 p(r[0],r[1],r[2]);
            points.push_back(p);
        }

        for (size_t i=0;i<tm->faces.size();i++)
        {
            MathTools::TriangleFace &f = tm->faces.at(i);
            std::vector<std::size_t> pol;
            pol.push_back(f.id1);
            pol.push_back(f.id2);
            pol.push_back(f.id3);
            polygons.push_back(pol);
        }

        CGAL::Polygon_mesh_processing::orient_polygon_soup(points, polygons);
        PolyhedronMeshPtr mesh(new PolyhedronMesh);
        CGAL::Polygon_mesh_processing::polygon_soup_to_polygon_mesh(points, polygons, *mesh);
        if (CGAL::is_closed(*mesh) && (!CGAL::Polygon_mesh_processing::is_outward_oriented(*mesh)))
          CGAL::Polygon_mesh_processing::reverse_face_orientations(*mesh);

        CGALPolyhedronMeshPtr res(new CGALPolyhedronMesh(mesh));

        return res;
    }*/

}
